import 'package:flutter/material.dart';
import 'package:manejod/drawerm.dart';
import 'package:manejod/view/log_in.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      initialRoute: "/",
      
      routes: {
        "/": (context)=> LogIn(),
        "drawe": (context)=> Drawerm(),
      },
      
      

    );
  }
}


 
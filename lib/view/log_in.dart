import 'package:flutter/material.dart';
import 'package:manejod/drawerm.dart';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  var controladorUsuario = TextEditingController();

  bool estadoCandado = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ClipOval(
              child: Container(
                  padding: EdgeInsets.all(5),
                  color: Colors.black,
                  child: FlutterLogo(
                    colors: Colors.red,
                    size: 123,
                  )),
            ),
            Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                child: Column(
                  children: <Widget>[
                    TextField(
                      onChanged: (cadena) {
                        print(cadena);
                      },
                      controller: controladorUsuario,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          labelText: "Usuario/Email",
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)))),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextField(
                      
                      keyboardType: TextInputType.visiblePassword,

                      obscureText: estadoCandado,
                      decoration: InputDecoration(
                        
                          prefixIcon: IconButton(
                              icon: estadoCandado? Icon(Icons.lock):Icon(Icons.lock_open),
                              onPressed: () {
                                setState(() {
                                  if (estadoCandado) {
                                     estadoCandado = false;
                                  } else {
                                    estadoCandado = true;
                                  }  
                                });
                                
                              }),

                          labelText: "Password",
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)))),
                    ),
                  ],
                )),
            Center(
              child: RaisedButton(
                onPressed: () {
                  String x1 = controladorUsuario.text.toString();
                  if (x1.length > 8) {
                    print(x1);
                    print(x1.length);
                    Navigator.of(context).pushNamed("drawe", arguments: x1);
                  }
                },

                color: Colors.red,
                //evento por controlador
                child: Text(
                  "Click me",
                  style: TextStyle(color: Colors.white),
                ),
                shape: StadiumBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class Prueba extends StatefulWidget {
  @override
  _PruebaState createState() => _PruebaState();
}

class _PruebaState extends State<Prueba> {
  String texto = "primer valor";
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
      
    );
  }

  Widget body(){
    return Container(
      child: Column(
        children: <Widget>[
          Text(texto, style: TextStyle(fontSize: 25),),
          TextField(
            onChanged: (cadena){

              setState(() {
                texto = cadena;
                print(texto);                
              });

            },
          ),
        ],
      ),
    );
  }
}